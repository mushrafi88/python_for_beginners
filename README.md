# python_for_beginners

install python and set the path variable in the os

# windows
when installing the python.exe tick the 'add path variable to path' option and install the pip.
possible installation locations 
C:\Users\username\AppData\Local\Programs
In case not done, do the following
1. Find the python installation folder and copy it 
2. search for edit path environment variables from the search box
3. Then Environment variables > System variables > Path > Edit > New > 'paste the path location'

# setting up a virtual environment

```
mkdir SOMETHING
cd SOMETHING
python -m venv <virtual_env>
cd <virtual_env>\Scripts
activate.bat
```
to return to normal
```
deactivate.bat
```
to setup an environment for a lower version of python
use the virtualenv

```
pip install virtualenv
py -m virtualenv -p=<python_ex> <virtuall_env_dir>
```

practice material
1. [jovian basic python practice](https://jovian.ai/akashreveluv/assignment-1-python-basics-practice)
2. [geeksforgeeks python basic practice](https://www.geeksforgeeks.org/python-exercises-practice-questions-and-solutions/)
